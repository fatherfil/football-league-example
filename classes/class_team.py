class Team(object):

    def __init__(self, name, teamId):
        self._id = teamId
        self._name = name
        self._gamesPlayed = 0
        self._points = 0
        self._goalsFor = 0
        self._goalsAgainst = 0
        self._goalDifference = 0

    @property
    def teamID(self):
        return self._id

    @property
    def points(self):
        return self._points

    @property
    def name(self):
        return self._name

    @property
    def goalDifference(self):
        return self._goalDifference

    def addPoints(self, points):
        self._points += points

    def addGoals(self, goalsFor, goalsAgainst):
        self._goalsFor += int(goalsFor)
        self._goalsAgainst += int(goalsAgainst)
        self._goalDifference = self._goalsFor - self._goalsAgainst

    def incrementGamesPlayed(self):
        self._gamesPlayed += 1

    def outputLeagueXML(self):
        return ("<team>" +
               "<!-- team ID:" + self._id.__str__() + " -->" +
               "<name>" + self._name + "</name>" +
               "<played>" + self._gamesPlayed.__str__() + "</played>" +
               "<goals>" +
               "<for>" + self._goalsFor.__str__() + "</for>" +
               "<against>" + self._goalsAgainst.__str__() + "</against>" +
               "<difference>" + self._goalDifference.__str__() + "</difference>" +
               "</goals>" +
               "<points>" + self._points.__str__() + "</points>" +
               "</team>\n")
from classes.class_team import Team
from classes.class_fixture import Fixture
from enhancedminidom import minidom

class League(object):
    _teams = []
    _fixtures = []

    def __init__(self, name):
        self._name = name

    def returnTeams(self):
        return self._teams

    def returnLeague(self):
        outputXML = "<xml>\n"
        sortedTeams = sorted(self._teams, key=lambda x: (-x.points, -x.goalDifference, x.name))
        for team in sortedTeams:
            outputXML = outputXML + team.outputLeagueXML()
        outputXML = outputXML + "</xml>\n"
        return outputXML

    def getNodeValue(self, xmlElement):
        for obj in xmlElement:
            return obj.childNodes[0].nodeValue

    def returnCountOfTeams(self):
        return self._teams.__len__()

    def returnCountOfFixtures(self):
        return self._fixtures.__len__()

    def addTeamsFromXML(self, newteams):
        teamXML = minidom.parseString(newteams)
        teamList = teamXML.getElementsByTagName("Team")

        for teamElement in teamList:
            nameElement = teamElement.getElementsByTagName("Name")
            idElement = teamElement.getElementsByTagName("Team_Id")
            for nameObj in nameElement:
                name = nameObj.childNodes[0].nodeValue
            for idObj in idElement:
                teamId = idObj.childNodes[0].nodeValue
            newTeam = Team(name, teamId)
            self._teams.append(newTeam)

    def addResultsFromXML(self, results):
        resultsXML = minidom.parseString(results)
        matchList = resultsXML.getElementsByTagName("Match")

        for matchElement in matchList:
            fixtureId = self.getNodeValue(matchElement.getElementsByTagName("FixtureMatch_Id"))
            hometeamId = self.getNodeValue(matchElement.getElementsByTagName("HomeTeam_Id"))
            awayteamId = self.getNodeValue(matchElement.getElementsByTagName("AwayTeam_Id"))
            homegoals = self.getNodeValue(matchElement.getElementsByTagName("HomeGoals"))
            awaygoals = self.getNodeValue(matchElement.getElementsByTagName("AwayGoals"))
            newFixture = Fixture(fixtureId, hometeamId, awayteamId, homegoals, awaygoals)
            self._fixtures.append(newFixture)

    def findTeamInLeague(self, teamID):
        for team in self._teams:
            if team.teamID == teamID:
                return team

    def computePointsFromFixtures(self):
        for fixture in self._fixtures:

            homePoints = 0
            awayPoints = 0

            if fixture.homeGoals > fixture.awayGoals:
                homePoints = 3
            elif fixture.homeGoals < fixture.awayGoals:
                awayPoints = 3
            else:
                homePoints = 1
                awayPoints = 1

            for team in self._teams:
                if team.teamID == fixture.homeTeam:
                    team.addPoints(homePoints)
                    team.addGoals(fixture.homeGoals, fixture.awayGoals)
                    team.incrementGamesPlayed()
                elif team.teamID == fixture.awayTeam:
                    team.addPoints(awayPoints)
                    team.addGoals(fixture.awayGoals, fixture.homeGoals)
                    team.incrementGamesPlayed()

class Fixture(object):

    def __init__(self, fixtureId, home_id, away_id, home_goals, away_goals):
        self._id = fixtureId
        self._homeTeam = home_id
        self._awayTeam = away_id
        self._homeGoals = home_goals
        self._awayGoals = away_goals

    @property
    def homeTeam(self):
        return self._homeTeam

    @property
    def awayTeam(self):
        return self._awayTeam

    @property
    def homeGoals(self):
        return self._homeGoals

    @property
    def awayGoals(self):
        return self._awayGoals


from httplib2 import Http
from urllib.parse import urlencode
from urllib.parse import quote_plus

class WebEngine(object):
    apiKey = "EFRXNSONRTUVGRFUCXMCKFBJUGELHRFDBDJRXQEUAVZMUIAUPA"
    baseURL = "http://www.xmlsoccer.com/FootballDataDemo.asmx/"

    def buildURL(self, request, dataitems):
        returnURL = self.baseURL + request + "?ApiKey=" + self.apiKey
        for name, value in dataitems.items():
            returnURL = returnURL + "&" + name + "=" + value
        return returnURL

    def doRequest(self, url):
        webRequest = Http()
        resp, content = webRequest.request(url, "GET")
        return content

    def getTeams(self, league, season):
        print ("Requesting teams from league: " + league + " for season: " + season)
        urldata = {"league": quote_plus(league), "seasonDateString": season}
        url = self.buildURL("GetAllTeamsByLeagueAndSeason", urldata)
        return self.doRequest(url)

    def getResults(self, league, startdate, enddate):
        print ("Requesting fixtures for league: " + league + " between: " + startdate + " and: " + enddate)
        urldata = {"league": quote_plus(league), "startDateString": startdate, "endDateString": enddate}
        url = self.buildURL("GetHistoricMatchesByLeagueAndDateInterval", urldata)
        return self.doRequest(url)


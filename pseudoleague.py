from classes.class_league import League
from engines.webconnector import WebEngine

try:

    # Create a webEngine object and request a list of teams from the XML feed
    try:
        webRequest = WebEngine()
        teamsXML = webRequest.getTeams("Scottish Premier League", "1213")
    except:
        print ("Error: unable to connect to web server for teams.")
        raise

    # Check that the XML return actually contains a team
    if teamsXML.__str__().find("<Team_Id>") == -1:
        raise Exception("Error: XML returned no team data.")

    # Create a new League object and add the teams into it
    scottishLeague = League("Scottish Premier League 2012/13")
    scottishLeague.addTeamsFromXML(teamsXML)

    # Now check that the league contains teams
    if scottishLeague.returnCountOfTeams() < 1:
        raise Exception("Error: League does not contain any teams")

    # Output the league table as it stands with no results
    print (scottishLeague.returnLeague())

    # Now add in a set of results from August 2012
    try:
        resultsXML = webRequest.getResults("Scottish Premier League", "01-08-2012", "31-08-2012")
    except:
        print ("Error: unable to connect to web server for results.")
        raise

    if resultsXML.__str__().find("<Match>") == -1:
        raise Exception("Error: XML returned no match data.")

    scottishLeague.addResultsFromXML(resultsXML)

    # Check that the league now contains a set of fixtures
    if scottishLeague.returnCountOfTeams() < 1:
        raise Exceptions("Error: League does not contain any fixtures.")

    print ("Fixtures loaded: %d" % scottishLeague.returnCountOfFixtures())

    # Compute the points gained for each team from the fixtures loaded
    scottishLeague.computePointsFromFixtures()

    # Then output the league again with our results in it
    print (scottishLeague.returnLeague())

except Exception as e:
    print ("Unable to complete process.", str(e))
